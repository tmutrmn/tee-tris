import Phaser from 'phaser'
// import { centerGameObjects } from '../utils'

export default class extends Phaser.State {
  init () {}

  preload () {
		let preloadBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY + 100, 'preloaderBar');
		preloadBar.anchor.setTo(0.5, 0.5)
		this.load.setPreloadSprite(preloadBar);
		preloadBar.cropEnabled = true;

		this.load.image('electronicFarts', 'assets/images/electronic-farts.png')
		this.load.image('fultiMail', 'assets/images/fulti-mail.png')
		this.load.image('teeTris', 'assets/images/tee-tris.png')
		this.load.image('blockage', 'assets/images/blockage.png')

		this.load.bitmapFont('carrier_command', 'assets/fonts/bitmapFonts/carrier_command.png', 'assets/fonts/bitmapFonts/carrier_command.xml');
		this.load.image('bitmapFont', 'assets/fonts/bitmap-font_03.png');


		this.load.image('bgrImage', 'assets/bgr' + this.game.conf.Background + '.jpg')
		this.load.image('areaBgr', 'assets/images/area-bgr.png')

		this.load.image('startButton', 'assets/images/play-button.png')
		this.load.image('restartButton', 'assets/images/restart-button.png')
		this.load.image('menuButton', 'assets/images/menu-button.png')
		this.load.image('configButton', 'assets/images/config-button.png')
		this.load.image('keysButton', 'assets/images/keys-button.png')
		this.load.image('backButton', 'assets/images/back-button.png')
		this.load.image('quitButton', 'assets/images/quit-button.png')

		this.load.image('crossUp', 'assets/images/cross-up.png')
		this.load.image('crossRight', 'assets/images/cross-right.png')
		this.load.image('crossMiddle', 'assets/images/cross-middle.png')
		this.load.image('crossDown', 'assets/images/cross-down.png')
		this.load.image('crossLeft', 'assets/images/cross-left.png')

		this.load.spritesheet('crossCtrl', 'assets/images/cross-ctrl.png', 128, 128);

		this.load.spritesheet('buttonAB', 'assets/images/button.png', 65, 65);

		this.load.spritesheet('tiles', 'assets/images/tiles.png', 18, 18, 18)
  }

  create () {
		// this.state.start('Game')
		this.state.start('Splash')
  }
}
