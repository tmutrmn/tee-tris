import Phaser from 'phaser'

export default class extends Phaser.State {
	constructor() {
		super();
		this.orientated = null;
	}

	init() {
		this.game.input.maxPointers = 1;
		this.game.stage.disableVisibilityChange = true;
		if (this.game.device.desktop) {
			this.game.scale.scaleMode = Phaser.ScaleManager.NO_SCALE; // EXACT_FIT or SHOW_ALL , NO_SCALE
			//this.game.scale.setGameSize(320 * 3, 180 * 3);
			this.game.scale.setMinMax(360, 640, 540, 960);
			this.game.scale.pageAlignHorizontally = true;
			this.game.scale.pageAlignVertically = false;
			this.game.scale.forceOrientation(true, false);
			// this.game.scale.setResizeCallback(this.gameResized, this);
			this.game.stage.smoothed = false;
		} else {
			this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
			//this.game.scale.setGameSize(320 * 3, 180 * 3);
			this.game.scale.setMinMax(180, 320, 720, 1280);
			this.game.scale.pageAlignHorizontally = true;
			this.game.scale.pageAlignVertically = true;
			this.game.scale.forceOrientation(true, false);
			// this.game.scale.setResizeCallback(this.gameResized, this);
			/*this.game.scale.enterIncorrectOrientation.add(this.enterIncorrectOrientation, this);
			this.game.scale.leaveIncorrectOrientation.add(this.leaveIncorrectOrientation, this);*/
			this.game.stage.smoothed = false;
		}

		this.stage.backgroundColor = '#010209'
		this.stage.backgroundColor = '#02030c'
		this.stage.backgroundColor = '#0e1519'
		this.fontsReady = true;
	}

	preload() {
		/*let text = this.add.text(this.world.centerX, this.world.centerY, 'loading fonts', {
			font: '16px Arial',
			fill: '#dddddd',
			align: 'center'
		})
		text.anchor.setTo(0.5, 0.5)*/

		this.load.image('preloaderBar', 'assets/images/preloader-bar.png');
	}

	render() {
		// if (this.fontsReady) {
		this.state.start('Preload')
		// }
	}

	fontsLoaded() {

	}

	gameResized(width, height) {
		//  This could be handy if you need to do any extra processing if the game resizes.
		//  A resize could happen if for example swapping orientation on a device or resizing the browser window.
		//  Note that this callback is only really useful if you use a ScaleMode of RESIZE and place it inside your main game state.
	}
}
