import Phaser from 'phaser'


export default class extends Phaser.State {
	init() {
		this.game.conf = JSON.parse(localStorage.getItem('tee-tris_config'));
		if (this.game.conf == null) {
			this.game.conf = {
				Level: 1,
				MovementTime: 100,
				RotationTime: 100,
				ShowVirtualControls: 'Yes',
				Background: 1
			}
		}
	}

	preload() {
		this.game.load.image('bgrImage', 'assets/bgr' + this.game.conf.Background + '.jpg')
	}

	create() {
		this.bgrImg = this.game.add.image(0, 0, 'bgrImage');

		let _keysText = this.game.add.bitmapText(this.game.world.centerX, this.game.world.centerY - 128, 'carrier_command', 'KEYS:\n', 14);
		_keysText.smoothed = false;
		_keysText.anchor.setTo(0.5, 0.5)

		_keysText = this.game.add.bitmapText(32, this.game.world.centerY, 'carrier_command', 'move left: Left Arrow\n\nmove right: Right Arrow\n\nmove down: Down Arrow\n\n\n\nrotate right: Q\n\nrotate left: W\n\n\n\nno key configuration\n\noption for now!', 10);
		_keysText.smoothed = false;
		_keysText.anchor.setTo(0.0, 0.5)



		this.backButton = this.game.add.button(this.game.world.centerX, this.game.world.height - 48, 'backButton', this.back, this);
		this.backButton.anchor.setTo(0.5, 0.5);
		this.backButton.scale.setTo(0.4, 0.4);
		this.backButton.visible = true;
	}


	update() {
		if (this.game.input.keyboard.isDown(Phaser.Keyboard.ESC)) this.back();
	}

	back() {
		localStorage.setItem('tee-tris_config', JSON.stringify(this.game.conf));

		this.state.start('MainMenu')
	}

	defineKeys() {
		this.state.start('MainMenu')
	}
}
