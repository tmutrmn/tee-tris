import Phaser from 'phaser'


export default class extends Phaser.State {
	init() {
		this.game.conf = JSON.parse(localStorage.getItem('tee-tris_config'));
		if (this.game.conf == null) {
			this.game.conf = {
				Level: 1,
				MovementTime: 100,
				RotationTime: 100,
				ShowVirtualControls: 'Yes',
				Background: 1
			}
		}

	}

	preload() {
		this.game.load.image('bgrImage', 'assets/bgr' + this.game.conf.Background + '.jpg')
		this.load.image('arrowLeft', 'assets/images/arrow-left.png')
		this.load.image('arrowRight', 'assets/images/arrow-right.png')
	}


	create() {
		this.bgrImg = this.game.add.image(0, 0, 'bgrImage');

		this.title = this.add.sprite(this.game.world.width / 2, 48, 'teeTris')
		this.title.anchor.setTo(0.5, 0.5)
		this.title.scale.setTo(0.75, 0.75)
		this.title.alpha = 1.0;

		this.confText = this.game.add.bitmapText(this.game.world.centerX, 108, 'carrier_command', 'Game configuration', 14);
		this.confText.smoothed = false;
		this.confText.anchor.setTo(0.5)

		this.keysButton = this.game.add.button(this.game.world.centerX, this.game.world.height - 48 - 56, 'keysButton', this.defineKeys, this);
		this.keysButton.anchor.setTo(0.5, 0.5);
		this.keysButton.scale.setTo(0.4, 0.4);
		this.keysButton.visible = true;

		this.backButton = this.game.add.button(this.game.world.centerX, this.game.world.height - 48, 'backButton', this.back, this);
		this.backButton.anchor.setTo(0.5, 0.5);
		this.backButton.scale.setTo(0.4, 0.4);
		this.backButton.visible = true;

		/*** starting level ***/
		this.startingLevelText = this.game.add.bitmapText(this.game.world.centerX, 128 + 16, 'carrier_command', 'Starting level', 8);
		this.startingLevelText.smoothed = false;
		this.startingLevelText.anchor.setTo(0.5)


		this.levelText = this.game.add.bitmapText(this.game.world.centerX, 128 + 48, 'carrier_command', this.game.conf.Level, 12);
		this.levelText.smoothed = false;
		this.levelText.anchor.setTo(0.5)

		// this.conf.Level = parseInt(this.conf.Level);
		this.levelDec = this.game.add.button(80, 128 + 36, 'arrowLeft', null, this, 0, 1, 0, 1);
		this.levelDec.scale.setTo(0.75)
		this.levelDec.fixedToCamera = true;
		this.levelDec.events.onInputOver.add(() => { return false; });
		this.levelDec.events.onInputOut.add(() => { return false; });
		this.levelDec.events.onInputDown.add(() => {
			if (this.game.conf.Level > 1) this.game.conf.Level--; else this.game.conf.Level = this.game.conf.Level;
			this.levelText.text = '' + this.game.conf.Level + '';
		});
		this.levelDec.events.onInputUp.add(() => { return false; });

		this.levelInc = this.game.add.button(this.game.width - 80 - 22, 128 + 36, 'arrowRight', null, this, 0, 1, 0, 1);
		this.levelInc.scale.setTo(0.75)
		this.levelInc.fixedToCamera = true;
		this.levelInc.events.onInputOver.add(() => { return false; });
		this.levelInc.events.onInputOut.add(() => { return false; });
		this.levelInc.events.onInputDown.add(() => {
			if (this.game.conf.Level < 10) this.game.conf.Level++; else this.game.conf.Level = this.game.conf.Level;
			this.levelText.text = '' + this.game.conf.Level + '';
		});
		this.levelInc.events.onInputUp.add(() => { return false; });


		/*** show virtual  controls ***/
		// this.conf.ShowVirtualControls = true;
		this.virtualControlsText = this.game.add.bitmapText(this.game.world.centerX, 128 + 92, 'carrier_command', 'Show virtual controls', 8);
		this.virtualControlsText.smoothed = false;
		this.virtualControlsText.anchor.setTo(0.5)

		this.showVirtualControlsText = this.game.add.bitmapText(this.game.world.centerX, 128 + 96 + 32, 'carrier_command', this.game.conf.ShowVirtualControls, 10);
		this.showVirtualControlsText.smoothed = false;
		this.showVirtualControlsText.anchor.setTo(0.5)

		this.virtualCtrlDec = this.game.add.button(80, 128 + 96 + 16, 'arrowLeft', null, this, 0, 1, 0, 1);
		this.virtualCtrlDec.scale.setTo(0.75)
		this.virtualCtrlDec.fixedToCamera = true;
		this.virtualCtrlDec.events.onInputOver.add(() => { return false; });
		this.virtualCtrlDec.events.onInputOut.add(() => { return false; });
		this.virtualCtrlDec.events.onInputDown.add(() => {
			this.game.conf.ShowVirtualControls = 'No';
			this.showVirtualControlsText.text = '' + this.game.conf.ShowVirtualControls + '';
		});
		this.virtualCtrlDec.events.onInputUp.add(() => { return false; });

		this.virtualCtrlInc = this.game.add.button(this.game.width - 80 - 22, 128 + 96 + 16, 'arrowRight', null, this, 0, 1, 0, 1);
		this.virtualCtrlInc.scale.setTo(0.75)
		this.virtualCtrlInc.fixedToCamera = true;
		this.virtualCtrlInc.events.onInputOver.add(() => { return false; });
		this.virtualCtrlInc.events.onInputOut.add(() => { return false; });
		this.virtualCtrlInc.events.onInputDown.add(() => {
			this.game.conf.ShowVirtualControls = 'Yes';
			this.showVirtualControlsText.text = '' + this.game.conf.ShowVirtualControls + '';
		});
		this.virtualCtrlInc.events.onInputUp.add(() => { return false; });



		/*** movement timers ***/
		// this.movementTimerValue = 100;
		this.movementTimersText = this.game.add.bitmapText(this.game.world.centerX, this.game.world.centerY - 24, 'carrier_command', 'Movement delay', 8);
		this.movementTimersText.smoothed = false;
		this.movementTimersText.anchor.setTo(0.5)

		this.movementTimersValueText = this.game.add.bitmapText(this.game.world.centerX, this.game.world.centerY + 8, 'carrier_command', this.game.conf.MovementTime, 10);
		this.movementTimersValueText.smoothed = false;
		this.movementTimersValueText.anchor.setTo(0.5)

		this.movementTimersValueDec = this.game.add.button(80, this.game.world.centerY - 4, 'arrowLeft', null, this, 0, 1, 0, 1);
		this.movementTimersValueDec.scale.setTo(0.75);
		this.movementTimersValueDec.fixedToCamera = true;
		this.movementTimersValueDec.events.onInputOver.add(() => { return false; });
		this.movementTimersValueDec.events.onInputOut.add(() => { return false; });
		this.movementTimersValueDec.events.onInputDown.add(() => {
			if (this.game.conf.MovementTime > 50) this.game.conf.MovementTime -= 25; else this.game.conf.MovementTime = 50;
			this.movementTimersValueText.text = '' + this.game.conf.MovementTime + '';
		});
		this.movementTimersValueDec.events.onInputUp.add(() => { return false; });

		this.movementTimersValueInc = this.game.add.button(this.game.width - 80 - 22, this.game.world.centerY - 4, 'arrowRight', null, this, 0, 1, 0, 1);
		this.movementTimersValueInc.scale.setTo(0.75);
		this.movementTimersValueInc.fixedToCamera = true;
		this.movementTimersValueInc.events.onInputOver.add(() => { return false; });
		this.movementTimersValueInc.events.onInputOut.add(() => { return false; });
		this.movementTimersValueInc.events.onInputDown.add(() => {
			if (this.game.conf.MovementTime < 300) this.game.conf.MovementTime += 25; else this.game.conf.MovementTime = 300;
			this.movementTimersValueText.text = '' + this.game.conf.MovementTime + '';
		});
		this.movementTimersValueInc.events.onInputUp.add(() => { return false; });


		/*** rotatien timer ***/
		this.rotationTimersText = this.game.add.bitmapText(this.game.world.centerX, this.game.world.centerY - 24 + 76, 'carrier_command', 'Rotation delay', 8);		this.rotationTimersText.smoothed = false;
		this.rotationTimersText.anchor.setTo(0.5)

		this.rotationTimersValueText = this.game.add.bitmapText(this.game.world.centerX, this.game.world.centerY + 8 + 76, 'carrier_command', this.game.conf.RotationTime, 10);
		this.rotationTimersValueText.smoothed = false;
		this.rotationTimersValueText.anchor.setTo(0.5)

		this.rotationTimersValueDec = this.game.add.button(80, this.game.world.centerY - 4 + 76, 'arrowLeft', null, this, 0, 1, 0, 1);
		this.rotationTimersValueDec.scale.setTo(0.75);
		this.rotationTimersValueDec.fixedToCamera = true;
		this.rotationTimersValueDec.events.onInputOver.add(() => { return false; });
		this.rotationTimersValueDec.events.onInputOut.add(() => { return false; });
		this.rotationTimersValueDec.events.onInputDown.add(() => {
		if (this.game.conf.RotationTime > 50) this.game.conf.RotationTime -= 25; else this.game.conf.RotationTime = 50;
			this.rotationTimersValueText.text = '' + this.game.conf.RotationTime + '';
		});
		this.rotationTimersValueDec.events.onInputUp.add(() => { return false; });

		this.rotationTimersValueInc = this.game.add.button(this.game.width - 80 - 22, this.game.world.centerY - 4 + 76, 'arrowRight', null, this, 0, 1, 0, 1);
		this.rotationTimersValueInc.scale.setTo(0.75);
		this.rotationTimersValueInc.fixedToCamera = true;
		this.rotationTimersValueInc.events.onInputOver.add(() => { return false; });
		this.rotationTimersValueInc.events.onInputOut.add(() => { return false; });
		this.rotationTimersValueInc.events.onInputDown.add(() => {
		if (this.game.conf.RotationTime < 300) this.game.conf.RotationTime += 25; else this.game.conf.RotationTime = 300;
			this.rotationTimersValueText.text = '' + this.game.conf.RotationTime + '';
		});
		this.rotationTimersValueInc.events.onInputUp.add(() => { return false; });



		/*** background ***/
		this.backgroundChooseText = this.game.add.bitmapText(this.game.world.centerX, this.game.world.centerY + 52 + 76, 'carrier_command', 'Background', 8);
		this.backgroundChooseText.smoothed = false;
		this.backgroundChooseText.anchor.setTo(0.5)

		this.backgroundText = this.game.add.bitmapText(this.game.world.centerX, this.game.world.centerY + 84 + 76, 'carrier_command', this.game.conf.Background, 12);
		this.backgroundText.smoothed = false;
		this.backgroundText.anchor.setTo(0.5)

		this.backgroundValueDec = this.game.add.button(80, this.game.world.centerY + 72 + 76, 'arrowLeft', null, this, 0, 1, 0, 1);
		this.backgroundValueDec.scale.setTo(0.75);
		this.backgroundValueDec.fixedToCamera = true;
		this.backgroundValueDec.events.onInputOver.add(() => { return false; });
		this.backgroundValueDec.events.onInputOut.add(() => { return false; });
		this.backgroundValueDec.events.onInputDown.add(() => {
		if (this.game.conf.Background > 1) this.game.conf.Background--; else this.game.conf.Background = 1;
			this.backgroundText.text = '' + this.game.conf.Background + '';
		});
		this.backgroundValueDec.events.onInputUp.add(() => { return false; });

		this.backgroundValueInc = this.game.add.button(this.game.width - 80 - 22, this.game.world.centerY + 72 + 76, 'arrowRight', null, this, 0, 1, 0, 1);
		this.backgroundValueInc.scale.setTo(0.75);
		this.backgroundValueInc.fixedToCamera = true;
		this.backgroundValueInc.events.onInputOver.add(() => { return false; });
		this.backgroundValueInc.events.onInputOut.add(() => { return false; });
		this.backgroundValueInc.events.onInputDown.add(() => {
		if (this.game.conf.Background < 3) this.game.conf.Background++; else this.game.conf.Background = 3;
			this.backgroundText.text = '' + this.game.conf.Background + '';
		});
		this.backgroundValueInc.events.onInputUp.add(() => { return false; });
	}


	update() {
		if (this.game.input.keyboard.isDown(Phaser.Keyboard.ESC)) this.back();
	}

	back() {
		// this.game.conf.RotationTime = this.game.conf.MovementTime;
		localStorage.setItem('tee-tris_config', JSON.stringify(this.game.conf));

		this.state.start('MainMenu')
	}

	defineKeys() {
		this.state.start('Keys')
	}
}
