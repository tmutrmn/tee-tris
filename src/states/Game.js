/* globals __DEV__ */
import Phaser from 'phaser'
import Tetrominoes from '../objects/Tetrominoes'
import Block from '../objects/Block'

const W = 10;
const H = 21;





export default class extends Phaser.State {
	init() {}

	preload() {
		console.log(this.game.conf);
		/*
		this.conf = JSON.parse(localStorage.getItem('tee-tris_config'));
		//
		if (this.conf === null) {
			this.conf = {
				Level: 1,
				MovementTime: 100,
				RotationTime: 100,
				ShowVirtualControls: 'Yes',
				Background: 1
			}
		}
		this.load.image('bgrImage', 'assets/bgr' + this.conf.Background + '.jpg')
		*/
	}

	create() {
		this.time.advancedTiming = true;
		this.game.world.setBounds(0, 0, this.game.width, this.game.height);
		this.game.camera.bounds = new Phaser.Rectangle(0, 0, this.game.width, this.game.height);

		//this.conf = new Configuration(this);
		this.cursors = this.game.input.keyboard.createCursorKeys();											// define kyes
		this.dropButton = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
		this.rotLeftButton = this.game.input.keyboard.addKey(Phaser.Keyboard.Q);
		this.rotRightButton = this.game.input.keyboard.addKey(Phaser.Keyboard.W);

		this.movementTimer = 0;
		this.rotationTimer = 0;
		this.gameOver = false;

		this.bgrImg = this.game.add.image(0, 0, 'bgrImage');
		this.areaBgr = this.game.add.image(this.game.width / 4, this.game.height / 4 - 100, 'areaBgr');
		this.areaBgr.alpha = 0.66;

		this.tiles = this.game.make.sprite(0, 0, 'tiles', 17);		// tiles
		this.tiles1 = this.game.make.sprite(0, 0, 'tiles', 1);

		let bgrTex = this.game.add.renderTexture(W * 18, (H - 1) * 18);			// create bgr
		for (let j = 0; j < H - 1; j++) {
			for (let i = 0; i < W; i++) {
				bgrTex.renderXY(this.tiles, i * 18, j * 18, false);
			}
		}
		this.bgr = this.game.add.sprite(0, 0, bgrTex);		//bgrTex);
		this.bgr.x = this.game.width / 4;
		this.bgr.y = this.game.height / 4 - 100;

		this.area = [];			// init play area
		for (let j = 0; j < H; j++) {
			this.area[j] = new Array(W);
			for (let i = 0; i < W; i++) {
				if (j === H - 1) this.area[j][i] = 1;		// add 1 blocks to teh bottom
				else this.area[j][i] = 0;
			}
		}

		this.tetromino = new Tetrominoes();		// blocks
		this.block = null;

		this.currentPieceLayer = this.game.add.group();		// layer to contain current piece
		this.landedPiecesLayer = this.game.add.group();				// layer to contain landed pieces

		this.spawnNewPiece = true;		// spawn new piece?
		this.piece = 0;								// current piece no
		this.prevPiece = this.piece;	// prev piece no = current piece no
		this.rot = 0;									// current piece rot


		this.speed = this.game.conf.Level * 0.5;

		this.moveLeft = false;
		this.moveRight = false;
		this.moveDown = false;
		this.rotLeft = false;
		this.rotRight = false;



		this.game.score = 0;
		this.game.highScore = parseInt(JSON.parse(localStorage.getItem('tee-tris_highscore'))) || 0
		if (this.game.highScore == null) {
			this.game.highScore = 0;
		}



		this.hudLayer = this.game.add.group();		// layer for points and texts and what not
		this.hudLayer.fixedToCamera = true;

		let _highScore = this.game.make.bitmapText(0, 12, 'carrier_command', ' HIGHSCORE:  ' + this.game.highScore + ' ', 8);
		_highScore.smoothed = false;
		_highScore.anchor.setTo(0.0, 0.0)
		this.hudLayer.add(_highScore);

		let _score = this.game.make.bitmapText(this.game.width - 64, 12, 'carrier_command', 'SCORE:', 8);
		_score.smoothed = false;
		_score.anchor.setTo(1.0, 0.0)
		this.hudLayer.add(_score);

		this.scoreText = this.game.make.bitmapText(this.game.width, 12, 'carrier_command', ' 0 ', 8);
		this.scoreText.smoothed = false;
		this.scoreText.anchor.setTo(1.0, 0.0)
		this.hudLayer.add(this.scoreText);


		let _lines = this.game.make.bitmapText(this.game.width - 64, 32, 'carrier_command', 'LINES:', 8);
		_lines.smoothed = false;
		_lines.anchor.setTo(1.0, 0.0)
		this.hudLayer.add(_lines);

		this.linesText = this.game.make.bitmapText(this.game.width, 32, 'carrier_command', ' 0 ', 8);
		this.linesText.smoothed = false;
		this.linesText.anchor.setTo(1.0, 0.0)
		this.hudLayer.add(this.linesText);

		this.gameoverText = this.game.make.bitmapText(this.game.width / 2, this.game.height / 2, 'carrier_command', '', 16);
		this.gameoverText.smoothed = false;
		this.gameoverText.anchor.setTo(0.5, 0.5)
		this.gameoverText.text = "GAME OVER!";

		this.gameoverRetroFont = this.game.add.retroFont('bitmapFont', 18, 24, Phaser.RetroFont.TEXT_SET11, 13, 0, 0);
		this.gameoverRetroFont.text = "GAME OVER";
		this.gameoverTextImage = this.hudLayer.create(this.game.width / 2, -100, this.gameoverRetroFont);
		this.gameoverTextImage.smoothed = false;
		this.gameoverTextImage.anchor.set(0.5, 0.5);
		this.gameoverTextImage.scale.set(1.5, 1.5);

		this.restartButton = this.game.make.button(this.game.width / 2, this.bgr.y + this.bgr.height + 64, 'restartButton', this.restart, this);
		this.restartButton.anchor.setTo(0.5, 0.5);
		this.restartButton.scale.setTo(0.4, 0.4);
		this.hudLayer.add(this.restartButton);
		this.restartButton.visible = false;

		this.menuButton = this.game.add.button(this.game.world.centerX, this.bgr.y + this.bgr.height + 128, 'menuButton', this.gotoMenu, this);
		this.menuButton.anchor.setTo(0.5, 0.5);
		this.menuButton.scale.setTo(0.4, 0.4);
		this.menuButton.visible = false;


		this.buttonLayer = this.game.add.group();		// layer for points and texts and what not

		this.crossCtrl = this.buttonLayer.create(0, 0, 'crossCtrl', 0);

		// left cross-controller button
		this.buttonLeft = this.game.make.button(48, 20, 'crossLeft', null, this, 0, 1, 0, 1);
		this.buttonLeft.anchor.setTo(1.0, 0.0)
		this.buttonLeft.width = 72; this.buttonLeft.height = 64;
		this.buttonLeft.alpha = 0.1;
		this.buttonLeft.fixedToCamera = true;
		this.buttonLeft.events.onInputOver.add(() => { this.moveLeft = false; });
		this.buttonLeft.events.onInputOut.add(() => { this.moveLeft = false; });
		this.buttonLeft.events.onInputDown.add(() => {
			if (this.game.time.now > this.movementTimer) this.moveLeft = true;
			this.movementTimer = this.game.time.now + this.game.conf.MovementTime;
			this.crossCtrl.frame = 1;
		});
		this.buttonLeft.events.onInputUp.add(() => {
			this.moveLeft = false;
			this.crossCtrl.frame = 0;
		});
		this.buttonLayer.add(this.buttonLeft);

		// right cross-controller button
		this.buttonRight = this.game.make.button(47 + 33, 20, 'crossRight', null, this, 0, 1, 0, 1);
		this.buttonRight.anchor.setTo(0.0, 0.0)
		this.buttonRight.width = 72; this.buttonRight.height = 64;
		this.buttonRight.alpha = 0.1;
		this.buttonRight.fixedToCamera = true;
		this.buttonRight.events.onInputOver.add(() => { this.moveRight = false; });
		this.buttonRight.events.onInputOut.add(() => { this.moveRight = false; });
		this.buttonRight.events.onInputDown.add(() => {
			if (this.game.time.now > this.movementTimer) this.moveRight = true;
			this.movementTimer = this.game.time.now + this.game.conf.MovementTime;
			this.crossCtrl.frame = 2;
		});
		this.buttonRight.events.onInputUp.add(() => {
			this.moveRight = false;
			this.crossCtrl.frame = 0;
		});
		this.buttonLayer.add(this.buttonRight);

		// right cross-controller button
		this.buttonDown = this.game.make.button(64, 80, 'crossDown', null, this, 0, 1, 0, 1);
		this.buttonDown.anchor.setTo(0.5, 0.0)
		this.buttonDown.width = 64; this.buttonDown.height = 72;
		this.buttonDown.alpha = 0.1;
		this.buttonDown.fixedToCamera = true;
		this.buttonDown.events.onInputOver.add(() => { this.moveDown = false; });
		this.buttonDown.events.onInputOut.add(() => { this.moveDown = false; });
		this.buttonDown.events.onInputDown.add(() => {
			if (this.game.time.now > this.movementTimer) this.moveDown = true;
			this.movementTimer = this.game.time.now + this.game.conf.MovementTime;
			this.crossCtrl.frame = 3;
		});
		this.buttonDown.events.onInputUp.add(() => {
			this.moveDown = false;
			this.crossCtrl.frame = 0;
		});
		this.buttonLayer.add(this.buttonDown);


		// button A
		this.buttonA = this.game.make.button(240, 16, 'buttonAB', null, this, 0, 0, 1, 0);
		this.buttonA.fixedToCamera = true;
		this.buttonA.events.onInputOver.add(() => { this.rotRight = false; });
		this.buttonA.events.onInputOut.add(() => { this.rotRight = false; });
		this.buttonA.events.onInputDown.add(() => {
			if (this.game.time.now > this.rotationTimer) this.rotRight = true;
			this.rotationTimer = this.game.time.now + this.game.conf.RotationTime;
		});
		this.buttonA.events.onInputUp.add(() => { this.rotRight = false; });
		this.buttonLayer.add(this.buttonA);


		// button B
		this.buttonB = this.game.make.button(176, 56, 'buttonAB', null, this, 0, 0, 1, 0);
		this.buttonB.fixedToCamera = true;
		this.buttonB.events.onInputOver.add(() => { this.rotLeft = false; });
		this.buttonB.events.onInputOut.add(() => { this.rotLeft = false; });
		this.buttonB.events.onInputDown.add(() => {
			if (this.game.time.now > this.rotationTimer) this.rotLeft = true;
			this.rotationTimer = this.game.time.now + this.game.conf.RotationTime;
		});
		this.buttonB.events.onInputUp.add(() => { this.rotLeft = false; });
		this.buttonLayer.add(this.buttonB);


		this.buttonLayer.x = 24;
		this.buttonLayer.y = 460;
		if (this.game.conf.ShowVirtualControls === 'No') this.buttonLayer.visible = false;
		else if (this.game.conf.ShowVirtualControls === 'Yes') this.buttonLayer.visible = true;

		//console.log(this.game.conf)
	}


	update() {

		if (!this.gameOver) {

			if (this.spawnNewPiece) {
				while (this.piece === this.prevPiece) this.piece = this.game.rnd.integerInRange(0, 4);		// prevent creating two same pieces on the trot
				this.prevPiece = this.piece;
				this.rot = 0;
				this.block = new Block(this.game, this.bgr.x, 0, this.piece, this.rot);			//this.initNewPiece();
				this.spawnNewPiece = false;
			}


			if (!this.spawnNewPiece && this.block.block != null) {		// if (!this.spawnNewPiece && this.currentPiece != null) {
				this.currentPieceLayer.removeAll(true)

				this.block.updateBlock(this.bgr.x, this.bgr.y);
				this.block.checkIfCanMoveDown(this.area);		// this.checkIfCanMoveDown();

				if (!this.block.canMoveDown && this.block.block.pos.currY === 0) {		//  && !canMoveLeft && !canMoveRight		// if (!this.canMoveDown && this.currentPiece.pos.currY === 0) {		//  && !canMoveLeft && !canMoveRight
					this.gameOver = true;
					console.log("GAME OVER!")
					if (this.game.score > this.game.highScore) {
						localStorage.setItem('tee-tris_highscore', JSON.stringify(this.game.score));
					}
					this.buttonLayer.visible = false;
					this.landedPiecesLayer.alpha = 0.5;
					this.gameoverTextImage.y = this.bgr.y + this.bgr.height / 2;
					this.restartButton.visible = true;
					this.menuButton.visible = true;
				}



				if ((this.moveLeft || this.cursors.left.isDown) && this.block.canMoveLeft && this.game.time.now > this.movementTimer) {
					this.block.block.pos.nextX -= 1;
					this.movementTimer = this.game.time.now + this.game.conf.MovementTime;
				} else if ((this.moveRight || this.cursors.right.isDown) && this.block.canMoveRight && this.game.time.now > this.movementTimer) {
					this.block.block.pos.nextX += 1;
					this.movementTimer = this.game.time.now + this.game.conf.MovementTime;
				}

				this.block.checkIfCanMoveLeftOrRight(this.area)

				this.block.canRotateLeft = this.block.canMoveLeft;
				this.block.canRotateRight = this.block.canMoveRight;

				if ((this.rotLeft || this.rotLeftButton.isDown) && this.block.canRotateLeft && this.game.time.now > this.block.rotationTimer) {
					this.block.rotateLeft(this.game.time.now);
				} else if ((this.rotRight || this.rotRightButton.isDown) && this.block.canRotateRight && this.game.time.now > this.block.rotationTimer) {
					this.block.rotateRight(this.game.time.now);
				}


				if (this.block.canMoveLeft || this.block.canMoveRight) this.block.block.pos.screenX = this.bgr.x + this.block.block.pos.nextX * 18;
				else this.block.block.pos.screenX = this.bgr.x + this.block.block.pos.currX * 18;


				if (this.block.canMoveDown) {		// if (this.canMoveDown) {
					if (this.moveDown || this.cursors.down.isDown) this.block.block.pos.screenY += this.speed * 2.50;
					else this.block.block.pos.screenY += this.speed * 0.5;
					this.block.drawBlockOnScreen(this.currentPieceLayer, this.bgr.x, this.bgr.y)
				} else {		// cannot move down: move the pieces to the landedPieces layer
					this.block.moveToLandedPieces(this.bgr.x, this.bgr.y, this.area, this.currentPieceLayer, this.landedPiecesLayer)		//this.moveToLandedPieces()
					this.game.score++;
					this.scoreText.text = ' ' + this.game.score + ' ';
					this.block.resetNewPiece();
					this.spawnNewPiece = true;
				}

				this.clearLine()
			}		// !this.spawnNewPiece

		} else {		// !this.gameOver
			if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) || this.game.input.keyboard.isDown(Phaser.Keyboard.ENTER)) this.restart();
		}
	}


	clearLine() {
		for (let j = 0; j < H - 1; j++) {
			let clearLine = 0;
			for (let i = 0; i < W; i++) {
				if (this.area[j][i] !== 0) clearLine++;
			}
			if (clearLine >= W) {
				this.game.lines++;
				this.linesText.text = ' ' + this.game.lines + ' ';		// dropped lines
				this.area.splice(j, 1);		// remove the filled line sub-array from the array
				this.area.unshift([0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);		// add a new empty line
				this.landedPiecesLayer.removeAll();			// clear the landed layer...
				for (let j = 0; j < H - 1; j++) {		// ..and refill it
					for (let i = 0; i < W; i++) {
						if (this.area[j][i] !== 0) {
							this.landedPiecesLayer.create(this.bgr.x + (i * 18), this.bgr.y + (j * 18), 'tiles', this.area[j][i]);
						}
					}
				}
			}
		}

		this.game.score += (this.game.lines - this.game.prevLines) * 7;
		this.game.prevLines = this.game.lines;
		this.scoreText.text = ' ' + this.game.score + ' ';
	}





	restart() {
		this.game.world.removeAll();
		this.state.restart();
	}

	gotoMenu() {
		this.game.world.removeAll();
		this.state.start('MainMenu')
	}


	render() {
		if (__DEV__) {
			// this.game.debug.text('highscore: ' + this.game.highScore + '', 10, 20);
			//this.game.debug.text(this.time.fps + ' fps', 10, 20);
			// this.game.debug.text(this.time.suggestedFps + ' fps', 10, 40);
			// this.game.debug.spriteInfo(this.mushroom, 32, 32)
		}
	}
}
