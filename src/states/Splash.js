import Phaser from 'phaser'


export default class extends Phaser.State {
  init () {}

  preload () {
		let pres = this.add.sprite(this.game.world.centerX, -200, 'electronicFarts')
		pres.anchor.setTo(0.5, 0.5)
		pres.alpha = 0.0;
		pres.alpha = 1.0;

		let prod = this.add.sprite(this.game.world.width + 100, this.game.world.centerY - 64, 'fultiMail')
		prod.anchor.setTo(0.5, 0.5)
		prod.alpha = 0.0;
		pres.alpha = 1.0;

		let title = this.add.sprite(this.game.world.width / 2, this.game.world.centerY - 208, 'teeTris')
		title.anchor.setTo(0.5, 0.5)
		title.alpha = 0.0;
		pres.alpha = 1.0;


		let tween1 = this.game.add.tween(pres).to({ alpha: 1, y: this.game.world.centerY - 64 }, 1000, Phaser.Easing.Linear.None, true)

		tween1.onComplete.addOnce(() => {
				setTimeout(() => {
					let _presents = this.game.add.bitmapText(this.game.world.centerX, this.game.world.centerY + 64, 'carrier_command', 'presents', 10);
					_presents.smoothed = false;
					_presents.anchor.setTo(0.5)

					let tween2 = this.game.add.tween(pres).to({ x: -pres.width }, 1000, Phaser.Easing.Quadratic.In, true)
					tween2.onComplete.addOnce(() => {
						_presents.text = '';

						let tween3 = this.game.add.tween(prod).to({ alpha: 1, x: this.game.world.centerX }, 1000, Phaser.Easing.Quadratic.Out, true)
						tween3.onComplete.addOnce(() => {
							let tween4 = this.game.add.tween(prod).to({ alpha: 0, x: 0 }, 1000, Phaser.Easing.Quadratic.In, true)
							setTimeout(() => {
								tween4.onComplete.addOnce(() => {
									let tween5 = this.game.add.tween(title).to({ alpha: 1 }, 1000, Phaser.Easing.Quadratic.In, true)
									tween5.onComplete.addOnce(() => {
										this.state.start('MainMenu')
									}, this);
								}, this);
							}, 500);
						}, this);
					}, this);
				}, 500);
		}, this);

	}



  create () {
		// this.state.start('MainMenu')
		// setTimeout(() => this.state.start('Game'), 2000);
  }
}
