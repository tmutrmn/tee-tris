import Phaser from 'phaser'


export default class extends Phaser.State {
	init() {
		this.game.conf = JSON.parse(localStorage.getItem('tee-tris_config'));
		if (this.game.conf == null) {
			this.game.conf = {
				Level: 1,
				MovementTime: 100,
				RotationTime: 100,
				ShowVirtualControls: 'Yes',
				Background: 1
			}
		}
	}

	preload() {
		this.game.load.image('bgrImage', 'assets/bgr' + this.game.conf.Background + '.jpg')
	}

	create() {
		this.bgrImg = this.game.add.image(0, 0, 'bgrImage');

		this.blockage = this.add.sprite(this.game.world.width / 2, 0 - 200, 'blockage')
		this.blockage.scale.setTo(0.66, 0.66)
		this.blockage.anchor.setTo(0.5, 0.5)
		this.blockage.alpha = 0.0;

		this.startButton = this.game.add.button(this.game.world.centerX, this.game.world.centerY + 128, 'startButton', this.start, this);
		this.startButton.anchor.setTo(0.5, 0.5);
		this.startButton.scale.setTo(0.4, 0.4);
		this.startButton.visible = false;

		this.configButton = this.game.add.button(this.game.world.centerX, this.game.world.centerY + 192, 'configButton', this.gotoConfig, this);
		this.configButton.anchor.setTo(0.5, 0.5);
		this.configButton.scale.setTo(0.4, 0.4);
		this.configButton.visible = false;

		this.quitButton = this.game.add.button(this.game.world.centerX, this.game.world.centerY + 256, 'quitButton', this.quit, this);
		this.quitButton.anchor.setTo(0.5, 0.5);
		this.quitButton.scale.setTo(0.4, 0.4);
		this.quitButton.visible = false;

		this.title = this.add.sprite(this.game.world.width / 2, this.game.world.centerY - 208, 'teeTris')
		this.title.anchor.setTo(0.5, 0.5)
		this.title.alpha = 1.0;

		// let tween1 = this.game.add.tween(this.blockage).to({ alpha: 0.75, y: this.game.world.centerY - 32 }, 1000, Phaser.Easing.Circular.InOut, true)
		let tween1 = this.game.add.tween(this.blockage).to({ alpha: 0.75, y: this.game.world.centerY - 32 }, 1000, Phaser.Easing.Cubic.Out, true)

		tween1.onComplete.addOnce(() => {
			this.startButton.visible = true;
			this.configButton.visible = true;
			this.quitButton.visible = true;
		}, this);
	}

	update() {
		if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) || this.game.input.keyboard.isDown(Phaser.Keyboard.ENTER)) this.start();
	}

	start() {
		this.state.start('Game')
	}

	gotoConfig() {
		this.state.start('Config')
	}

	quit() {
		// this.state.start('Splash')
		if (navigator.app) {
			navigator.app.exitApp();
		} else if (navigator.device) {
			navigator.device.exitApp();
		} else {
			window.close();
		}
	}

}
