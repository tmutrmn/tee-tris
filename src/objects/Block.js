import Tetrominoes from '../objects/Tetrominoes'

export default class Block {
	constructor(game, x, y, piece, rot)  {
		this.tetromino = new Tetrominoes();
		this.game = game
		// console.log(this.game.conf)

		this.piece = piece;
		this.rot = rot;
		this.block = Object.assign({}, this.tetromino.block[piece]);
		this.block.pos.screenX = x + this.block.pos.currX * 18;
		this.block.pos.screenY = y + this.block.pos.currY * 18;

		this.block.pos.currX = Math.floor((this.block.pos.screenX - x) / 18);
		this.block.pos.currY = Math.floor((this.block.pos.screenY) / 18);
		this.block.pos.nextX = this.block.pos.currX;
		this.block.pos.nextY = this.block.pos.currY + 1;

		this.canMoveDown = true;
		this.canMoveLeft = true;
		this.canMoveRight = true;
		this.canRotateLeft = true;
		this.canRotateRight = true;

		this.rotationTimer = 0;
		this.movementTimer = 0;
	}


	resetNewPiece() {
		this.block.pos.currX = 4;		// reset current piece props
		this.block.pos.currY = 0;
		this.block.pos.nextX = 4;
		this.block.pos.nextY = 0;
		this.block.pos.screenX = 4 * 18;
		this.block.pos.screenY = 0;
		this.block = null;		// ???
		// this.spawnNewPiece = true;
		this.rot = 0;
	}


	drawBlockOnScreen(layer, x, y) {
		for (let j = 0; j < this.block.shapeRot[this.rot].length; j++) {
			for (let i = 0; i < this.block.shapeRot[this.rot][j].length; i++) {
				if (this.block.shapeRot[this.rot][j][i] !== 0) {
					layer.create(i * 18 + this.block.pos.screenX, y + j * 18 + this.block.pos.screenY, 'tiles', this.block.tile);
				}
			}
		}
	}


	updateBlock(x, y) {
		this.block.pos.currX = Math.floor((this.block.pos.screenX - x) / 18);
		this.block.pos.currY = Math.floor((this.block.pos.screenY) / 18);
		this.block.pos.nextX = this.block.pos.currX;
		this.block.pos.nextY = this.block.pos.currY + 1;

		this.canMoveDown = true;
		this.canMoveLeft = true;
		this.canMoveRight = true;
		this.canRotateLeft = true;
		this.canRotateRight = true;
	}



	checkIfCanMoveDown(area) {
		for (let j = 0; j < this.block.shapeRot[this.rot].length; j++) {
			for (let i = 0; i < this.block.shapeRot[this.rot][j].length; i++) {
				if (this.block.shapeRot[this.rot][j][i] !== 0) {
					if (area[j + this.block.pos.currY][i + this.block.pos.nextX] !== 0 || area[j + this.block.pos.nextY][i + this.block.pos.nextX] !== 0) {
						this.canMoveDown = false;
					}
				}
			}
		}
	}


	rotateLeft(time) {
		let maxRot = this.block.shapeRot.length;
		let nextRot = this.rot + 1; nextRot = nextRot % maxRot;

		if (this.block.shapeRot[nextRot][0].length === 2 && this.block.pos.nextX + this.block.shapeRot[nextRot][0].length === 11) {
			this.block.pos.currX--;
			this.rot++; this.rot = this.rot % maxRot;
			this.rotationTimer = time + this.game.conf.RotationTime;
			this.canRotateLeft = false;
			this.canRotateRight = false;
		} else if (this.block.pos.nextX + this.block.shapeRot[nextRot][0].length <= 10) {
			this.rot++; this.rot = this.rot % maxRot;
			this.rotationTimer = time + this.game.conf.RotationTime;
			this.canRotateLeft = false;
			this.canRotateRight = false;
		}
		if (this.block.pos.nextX < 0) {
			this.block.pos.nextX = 0;
		}
	}

	rotateRight(time) {
		let maxRot = this.block.shapeRot.length;
		let nextRot = this.rot - 1; if (nextRot < 0) nextRot = maxRot - 1;

		if (this.block.shapeRot[nextRot][0].length === 2 && this.block.pos.nextX + this.block.shapeRot[nextRot][0].length === 11) {
			this.block.pos.currX--;
			this.rot--; if (this.rot < 0) this.rot = maxRot - 1;
			this.rotationTimer = time + this.game.conf.RotationTime;
			this.canRotateLeft = false;
			this.canRotateRight = false;
		} else if (this.block.pos.nextX + this.block.shapeRot[nextRot][0].length <= 10) {
			this.rot--; if (this.rot < 0) this.rot = maxRot - 1;
			this.rotationTimer = time + this.game.conf.RotationTime;
			this.canRotateLeft = false;
			this.canRotateRight = false;
		}
		if (this.block.pos.nextX < 0) {
			this.block.pos.nextX = 0;
		}
	}


	checkIfCanMoveLeftOrRight(area) {
		for (let j = 0; j < this.block.shapeRot[this.rot].length; j++) {
			for (let i = 0; i < this.block.shapeRot[this.rot][j].length; i++) {
				if (this.block.shapeRot[this.rot][j][i] !== 0) {
					if (area[j + this.block.pos.currY][i + this.block.pos.nextX] !== 0 || area[j + this.block.pos.nextY][i + this.block.pos.nextX] !== 0) {
						this.canMoveLeft = false;
						this.canMoveRight = false;
					}
				}
			}
		}
	}


	moveToLandedPieces(x, y, area, layer, landedLayer) {
		for (let j = 0; j < this.block.shapeRot[this.rot].length; j++) {
			for (let i = 0; i < this.block.shapeRot[this.rot][j].length; i++) {
				if (this.block.shapeRot[this.rot][j][i] !== 0) {
					area[this.block.pos.currY + j][this.block.pos.currX + i] = this.block.tile;
					layer.create(x + (i + this.block.pos.currX) * 18, y + (j + this.block.pos.currY) * 18, 'tiles', this.block.tile);
				}
			}
		}
		layer.moveAll(landedLayer);
	}

}
