export default class {			// silly foolishness! should be done some fancy bitshifted arrays and funky bitwise operators! but im lazy!
	constructor() {
		this.block = [];
		this.block[0] = {
			height: 2,
			width: 2,
			pos: {
				currX: 4,
				currY: 0,
				nextX: 4,
				nextY: 1,
				screenX: 4 * 18,
				screenY: 0 * 18
			},
			tile: 9,
			rot: 0,
			rotOffset: 0,
			shapeRot: [
				[
					[1, 1],
					[1, 1]
				],
				[
					[1, 1],
					[1, 1]
				]
			]
		};
		this.block[1] = {
			height: 3,
			width: 2,
			pos: {
				currX: 4,
				currY: 0,
				nextX: 4,
				nextY: 1,
				screenX: 4 * 18,
				screenY: 0 * 18
			},
			tile: 10,
			rot: 0,
			rotOffset: 0,
			shapeRot: [
				[
					[0, 1],
					[0, 1],
					[1, 1]
				],
				[
					[1, 0, 0],
					[1, 1, 1]
				],
				[
					[1, 1],
					[1, 0],
					[1, 0]
				],
				[
					[1, 1, 1],
					[0, 0, 1]
				]
			]
		};
		this.block[2] = {
			height: 3,
			width: 2,
			pos: {
				currX: 4,
				currY: 0,
				nextX: 4,
				nextY: 1,
				screenX: 4 * 18,
				screenY: 0 * 18
			},
			tile: 11,
			rot: 0,
			rotOffset: 0,
			shapeRot: [
				[
					[1, 0],
					[1, 0],
					[1, 1]
				],
				[
					[1, 1, 1],
					[1, 0, 0]
				],
				[
					[1, 1],
					[0, 1],
					[0, 1]
				],
				[
					[0, 0, 1],
					[1, 1, 1]
				]
			]
		};
		this.block[3] = {
			height: 4,
			width: 3,
			pos: {
				currX: 3,
				currY: 0,
				nextX: 3,
				nextY: 1,
				screenX: 3 * 18,
				screenY: 0 * 18
			},
			tile: 12,
			rot: 0,
			rotOffset: 2,
			shapeRot: [
				[
					[0, 1, 0],
					[0, 1, 0],
					[0, 1, 0],
					[0, 1, 0]
				],
				[
					[0, 0, 0, 0],
					[1, 1, 1, 1],
					[0, 0, 0, 0]
				]
			]
		};
		this.block[4] = {
			height: 2,
			width: 3,
			pos: {
				currX: 4,
				currY: 0,
				nextX: 4,
				nextY: 1,
				screenX: 4 * 18,
				screenY: 0 * 18
			},
			tile: 13,
			rot: 0,
			rotOffset: 0,
			shapeRot: [
				[
					[1, 1, 1],
					[0, 1, 0]
				],
				[
					[0, 1],
					[1, 1],
					[0, 1]
				],
				[
					[0, 1, 0],
					[1, 1, 1]
				],
				[
					[1, 0],
					[1, 1],
					[1, 0]
				]
			]
		};

	}
}
