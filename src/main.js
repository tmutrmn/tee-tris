import 'pixi'
import 'p2'
import Phaser from 'phaser'

import BootState from './states/Boot'
import PreloadState from './states/Preload'
import SplashState from './states/Splash'
import MainMenuState from './states/MainMenu'
import ConfigurationState from './states/Configuration'
import KeysState from './states/Keys'
import GameState from './states/Game'

import config from './config'		// main.js config

class Game extends Phaser.Game {
	constructor() {
		const docElement = document.documentElement
		const width = docElement.clientWidth > config.gameWidth ? config.gameWidth : docElement.clientWidth
		const height = docElement.clientHeight > config.gameHeight ? config.gameHeight : docElement.clientHeight

		super(width, height, Phaser.CANVAS, 'content', null)

		this.state.add('Boot', BootState, false)
		this.state.add('Preload', PreloadState, false)
		this.state.add('Splash', SplashState, false)
		this.state.add('MainMenu', MainMenuState, false)
		this.state.add('Config', ConfigurationState, false)
		this.state.add('Keys', KeysState, false)
		this.state.add('Game', GameState, false)


		this.conf = JSON.parse(localStorage.getItem('tee-tris_config'));
		console.log(this.conf);
		if (this.conf == null) {
			this.conf = {
				Level: 1,
				MovementTime: 100,
				RotationTime: 100,
				ShowVirtualControls: 'Yes',
				Background: 1
			}
		}

		this.highScore = parseInt(JSON.parse(localStorage.getItem('tee-tris_highscore'))) || 0
		if (this.highScore == null) {
			this.highScore = 0;
		}

		this.score = 0;
		this.lines = 0;
		this.prevLines = this.lines;

		this.state.start('Boot')
	}
}

window.game = new Game()
