# Tee-Tris Tetris-clone.
#### Made with Phaser + ES6 + Webpack.

Tee-Tris a simple tetris-clone
https://gitlab.com/tmutrmn/tee-tris/

Made with Phaser
http://phaser.io/

based on Phaser-ES6 boilerplate:
https://github.com/lean/phaser-es6-webpack

## 1. Install node.js and npm:

https://nodejs.org/en/


## 2. Install dependencies (optionally you could install [yarn](https://yarnpkg.com/)):

Navigate to the cloned repo’s directory.

Run:

```npm install``` 

## 3. Run the development server:

Run:

```npm run dev```

This will run a server so you can run the game in a browser.

Open your browser and enter localhost:3000 into the address bar.

Also this will start a watch process, so you can change the source and the process will recompile and refresh the browser


## Build for deployment:

Run:

```npm run deploy```

This will optimize and minimize the compiled bundle.

## Credits

http://phaser.io/

https://github.com/lean

https://github.com/lean/phaser-es6-webpack
